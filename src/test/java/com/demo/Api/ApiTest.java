package com.demo.Api;

import com.demo.Api.Entity.Adhaar;
import com.demo.Api.Repository.AdhaarRepo;
import com.demo.Api.Service.AdhaarService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApiTest {

    @Autowired
    AdhaarService service;

    @MockBean
    AdhaarRepo repo;



    @Test
    public void saveAdhaar(){
        Adhaar adhaar = new Adhaar("123456789012","Mano");
        when(repo.save(adhaar)).thenReturn(adhaar);
        assertEquals(adhaar,service.saveDetails(adhaar));
    }


    @Test
    public void getAdhaarTest() {
        when(repo.findAll()).thenReturn(Stream
                .of(new Adhaar("574609268976", "pkachu"), new Adhaar("966072314238", "Bada bhai")).collect(Collectors.toList()));
        assertEquals(2, service.getAllDetails().size());
    }


    @Test
    public void getDetailsByAdhaarNumber() {
        String adhaarNumber = "123456789103";
        Optional<Adhaar> adhaar = Optional.of(new Adhaar("123456789103", "Mano"));
        when(repo.findById(adhaarNumber)).thenReturn(adhaar);
        assertEquals(adhaarNumber, service.getAdhaarDetails(adhaarNumber).get().getAdhaarNumber());
    }

}
