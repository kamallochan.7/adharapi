package com.demo.Api.Service;


import com.demo.Api.Entity.Adhaar;
import com.demo.Api.Repository.AdhaarRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AdhaarService {


    @Autowired
    AdhaarRepo adhaarRepo;

    public Optional<Adhaar> getAdhaarDetails(String adhaar){
        return adhaarRepo.findById(adhaar);
    }


    public List<Adhaar> getAllDetails(){
        List<Adhaar> adhaar = adhaarRepo.findAll();
        return adhaar;
    }


    public Adhaar saveDetails(Adhaar adhaar){
        return adhaarRepo.save(adhaar);
    }


}
