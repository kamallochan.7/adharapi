package com.demo.Api.Repository;

import com.demo.Api.Entity.Adhaar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AdhaarRepo extends JpaRepository<Adhaar, String>{

//    Optional<Adhaar> findByAdharNum(String adhaar);
}
