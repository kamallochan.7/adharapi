package com.demo.Api.Controller;


import com.demo.Api.Entity.Adhaar;
import com.demo.Api.Service.AdhaarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class AdhaarController {

    @Autowired
    public AdhaarService service;


    @PostMapping("/save")
    public ResponseEntity<Adhaar> saveDetails(@RequestBody Adhaar adhaar){
        return ResponseEntity.ok(service.saveDetails(new Adhaar (adhaar.getAdhaarNumber(), adhaar.getName())));
    }


    @GetMapping("/")
    public List<Adhaar> getAlldetails(){
        return service.getAllDetails();
    }

    @GetMapping("/{adhaarNumber}")
//    public ResponseEntity<Adhaar> getAdhaarById(@PathVariable("adhaarNumber") String adhaarNumber) {
//
//        Optional<Adhaar> adhaardetails = service.getAdhaarDetails(adhaarNumber);
//        return new ResponseEntity< >((adhaardetails.get), HttpStatus.OK);

    public ResponseEntity<?> getAdhaarById (@PathVariable("adhaarNumber") String adhaarNumber){
        return ResponseEntity.ok(service.getAdhaarDetails(adhaarNumber));
    }


}
